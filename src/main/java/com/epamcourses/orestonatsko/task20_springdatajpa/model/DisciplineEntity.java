package com.epamcourses.orestonatsko.task20_springdatajpa.model;


import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "discipline", schema = "student_db")
public class DisciplineEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "discipline_id", nullable = false)
    private Long id;
    @Column(name = "discipline")
    private String discipline;

    @ManyToMany(mappedBy = "disciplineList")
    private Set<StudentEntity> students;

    public DisciplineEntity() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDiscipline() {
        return discipline;
    }

    public void setDiscipline(String discipline) {
        this.discipline = discipline;
    }

    @Override
    public String toString() {
        return String.format("%-5d %s", id, discipline);
    }
}
