package com.epamcourses.orestonatsko.task20_springdatajpa.model;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "st_group", schema = "student_db")
public class GroupEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "group_id")
    private Long groupId;

    @Column(name = "name")
    private String name;

    @OneToMany(mappedBy = "group")
    private List<StudentEntity> students;

    public GroupEntity() {
    }

    public Long getId() {
        return groupId;
    }

    public void setId(Long id) {
        this.groupId = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<StudentEntity> getStudents() {
        return students;
    }

    public void setStudents(List<StudentEntity> students) {
        this.students = students;
    }

    @Override
    public String toString() {
        return String.format("%-5d %s", groupId, name);
    }


}
