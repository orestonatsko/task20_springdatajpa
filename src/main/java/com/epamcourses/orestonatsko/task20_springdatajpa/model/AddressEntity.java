package com.epamcourses.orestonatsko.task20_springdatajpa.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "address",schema = "student_db")
public class AddressEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "address_id")
    private Long addressId;

    @Column(name = "city")
    private String city;

    @Column(name = "street")
    private String street;

    @Column(name = "numb_building")
    private Integer numbBuilding;

    @JsonIgnore
    @OneToMany(mappedBy = "address")
    private List<StudentEntity> students;

    public AddressEntity() {
    }

    public Long getId() {
        return addressId;
    }

    public void setId(Long id) {
        this.addressId = id;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public Integer getNumbBuilding() {
        return numbBuilding;
    }

    public void setNumbBuilding(Integer numbBuilding) {
        this.numbBuilding = numbBuilding;
    }

    public List<StudentEntity> getStudents() {
        return students;
    }

    public void setStudents(StudentEntity student) {
        this.students.add(student);
    }

    @Override
    public String toString() {
        return String.format("%-5d %-15s %-18s %-15d", addressId, city, street, numbBuilding);
    }
}
