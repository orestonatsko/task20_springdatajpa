package com.epamcourses.orestonatsko.task20_springdatajpa.model;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "student", schema = "student_db")
public class StudentEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "student_id")
    private Long id;

    @Column(name = "first_name")
    private String firstName;

    @Column(name = "second_name")
    private String secondName;

   @ManyToOne
   @JoinColumn(name = "group_id", referencedColumnName = "group_id")
    private GroupEntity group;

    @ManyToOne
    @JoinColumn(name = "address_id", referencedColumnName = "address_id")
    private AddressEntity address;

    @ManyToMany
    @JoinTable(name = "student_discipline", schema = "student_db",
            joinColumns = @JoinColumn(name = "student_id", referencedColumnName = "student_id", nullable = false),
            inverseJoinColumns = @JoinColumn(name = "discipline_id", referencedColumnName = "discipline_id", nullable = false))
    private List<DisciplineEntity> disciplineList;

    public StudentEntity() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getSecondName() {
        return secondName;
    }

    public void setSecondName(String secondName) {
        this.secondName = secondName;
    }

    public GroupEntity getGroup() {
        return group;
    }

    public void setGroup(GroupEntity groupId) {
        this.group = groupId;
    }

    public AddressEntity getAddress() {
        return address;
    }

    public void setAddress(AddressEntity addressId) {
        this.address = addressId;
    }

    @Override
    public String toString() {
        return String.format("%-5d %-15s %-15s %-15s %-15s", id, firstName, secondName,
        address== null?"null":address.getCity(), group== null? "null": group.getName());
    }
}
