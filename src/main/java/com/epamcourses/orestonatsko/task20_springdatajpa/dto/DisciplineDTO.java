package com.epamcourses.orestonatsko.task20_springdatajpa.dto;

import com.epamcourses.orestonatsko.task20_springdatajpa.model.DisciplineEntity;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.ResourceSupport;

public class DisciplineDTO extends ResourceSupport {
    private DisciplineEntity discipline;

    public DisciplineDTO(DisciplineEntity discipline, Link selfLink) {
        this.discipline = discipline;
        add(selfLink);
    }

    public Long getDisciplineId(){
        return discipline.getId();
    }

    public String getDiscipline(){
        return discipline.getDiscipline();
    }
}
