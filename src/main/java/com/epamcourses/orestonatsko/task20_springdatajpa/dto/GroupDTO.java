package com.epamcourses.orestonatsko.task20_springdatajpa.dto;

import com.epamcourses.orestonatsko.task20_springdatajpa.controllers.StudentController;
import com.epamcourses.orestonatsko.task20_springdatajpa.exceptions.NoSuchAddressException;
import com.epamcourses.orestonatsko.task20_springdatajpa.exceptions.NoSuchGroupException;
import com.epamcourses.orestonatsko.task20_springdatajpa.exceptions.NoSuchStudentException;
import com.epamcourses.orestonatsko.task20_springdatajpa.model.GroupEntity;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.ResourceSupport;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

public class GroupDTO  extends ResourceSupport {
    private GroupEntity group;

    public GroupDTO(GroupEntity group, Link selfLink) throws NoSuchStudentException, NoSuchAddressException, NoSuchGroupException {
        this.group = group;
        add(selfLink);
        add(linkTo(methodOn(StudentController.class).getStudentsByGroupId(group.getId())).withRel("student"));
    }

    public String getGroup(){
        return group.getName();
    }
}
