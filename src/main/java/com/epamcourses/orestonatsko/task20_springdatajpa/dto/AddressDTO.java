package com.epamcourses.orestonatsko.task20_springdatajpa.dto;


import com.epamcourses.orestonatsko.task20_springdatajpa.controllers.StudentController;
import com.epamcourses.orestonatsko.task20_springdatajpa.exceptions.NoSuchAddressException;
import com.epamcourses.orestonatsko.task20_springdatajpa.exceptions.NoSuchGroupException;
import com.epamcourses.orestonatsko.task20_springdatajpa.exceptions.NoSuchStudentException;
import com.epamcourses.orestonatsko.task20_springdatajpa.model.AddressEntity;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.ResourceSupport;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

public class AddressDTO extends ResourceSupport {
    private AddressEntity address;

    public AddressDTO(AddressEntity address, Link selfLink) throws NoSuchAddressException, NoSuchStudentException, NoSuchGroupException {
        this.address = address;
        add(selfLink);
            add(linkTo(methodOn(StudentController.class).getStudentsByAddressId(address.getId())).withRel("student"));
    }

    public Integer getNumberBuilding() {
        return address.getNumbBuilding();
    }

    public String getCity() {
        return address.getCity();
    }

    public String getStreet() {
        return address.getStreet();
    }


}
