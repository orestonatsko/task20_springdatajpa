package com.epamcourses.orestonatsko.task20_springdatajpa.dto;

import com.epamcourses.orestonatsko.task20_springdatajpa.controllers.AddressController;
import com.epamcourses.orestonatsko.task20_springdatajpa.controllers.GroupController;
import com.epamcourses.orestonatsko.task20_springdatajpa.exceptions.NoSuchAddressException;
import com.epamcourses.orestonatsko.task20_springdatajpa.exceptions.NoSuchGroupException;
import com.epamcourses.orestonatsko.task20_springdatajpa.exceptions.NoSuchStudentException;
import com.epamcourses.orestonatsko.task20_springdatajpa.model.AddressEntity;
import com.epamcourses.orestonatsko.task20_springdatajpa.model.GroupEntity;
import com.epamcourses.orestonatsko.task20_springdatajpa.model.StudentEntity;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.ResourceSupport;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

public class StudentDTO  extends ResourceSupport {
    private StudentEntity student;

    public StudentDTO(StudentEntity student, Link selfLink) throws NoSuchStudentException, NoSuchGroupException, NoSuchAddressException {
        this.student = student;
        add(selfLink);
        add(linkTo(methodOn(AddressController.class).getAddressByStudentId(student.getId())).withRel("address"));
        add(linkTo(methodOn(GroupController.class).getGroupByStudentId(student.getId())).withRel("group"));
    }

    Long getStudentId(){
        return student.getId();
    }

    public String getStudentFirstName(){
        return student.getFirstName();
    }

    public String getStudentSecondName(){
        return student.getSecondName();
    }

    GroupEntity getStudentGroup(){
        return student.getGroup();
    }

    AddressEntity getStudentAdress(){
        return student.getAddress();
    }


}
