package com.epamcourses.orestonatsko.task20_springdatajpa.repositories;

import com.epamcourses.orestonatsko.task20_springdatajpa.model.StudentEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface StudentRepository extends JpaRepository<StudentEntity, Long> {
    List<StudentEntity> findByFirstNameLike(String like);
}
