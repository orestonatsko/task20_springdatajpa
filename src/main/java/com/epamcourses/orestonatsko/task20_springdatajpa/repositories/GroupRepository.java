package com.epamcourses.orestonatsko.task20_springdatajpa.repositories;

import com.epamcourses.orestonatsko.task20_springdatajpa.model.GroupEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface GroupRepository extends JpaRepository<GroupEntity, Long> {
}
