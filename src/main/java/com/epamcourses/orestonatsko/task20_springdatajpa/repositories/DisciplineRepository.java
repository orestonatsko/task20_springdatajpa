package com.epamcourses.orestonatsko.task20_springdatajpa.repositories;

import com.epamcourses.orestonatsko.task20_springdatajpa.model.DisciplineEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DisciplineRepository extends JpaRepository<DisciplineEntity, Long> {
}
