package com.epamcourses.orestonatsko.task20_springdatajpa.repositories;

import com.epamcourses.orestonatsko.task20_springdatajpa.model.AddressEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AddressRepository extends JpaRepository<AddressEntity, Long> {

    List<AddressEntity> findAddressByCity(String city);
}
