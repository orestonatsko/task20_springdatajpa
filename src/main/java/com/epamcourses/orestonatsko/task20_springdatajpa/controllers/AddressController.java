package com.epamcourses.orestonatsko.task20_springdatajpa.controllers;

import com.epamcourses.orestonatsko.task20_springdatajpa.dto.AddressDTO;
import com.epamcourses.orestonatsko.task20_springdatajpa.exceptions.NoSuchAddressException;
import com.epamcourses.orestonatsko.task20_springdatajpa.exceptions.NoSuchGroupException;
import com.epamcourses.orestonatsko.task20_springdatajpa.exceptions.NoSuchStudentException;
import com.epamcourses.orestonatsko.task20_springdatajpa.model.AddressEntity;
import com.epamcourses.orestonatsko.task20_springdatajpa.services.AddressService;
import com.epamcourses.orestonatsko.task20_springdatajpa.services.implementation.AddressServiceImpl;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

@RestController
@RequestMapping("/address")
public class AddressController {

    private AddressService addressService;

    public AddressController(AddressServiceImpl addressService) {
        this.addressService = addressService;
    }

    @GetMapping("/student/{studentId}")
    public AddressEntity getAddressByStudentId(@PathVariable Long studentId) throws NoSuchStudentException {
        return addressService.findAddressByStudentId(studentId);
    }

    @GetMapping({"", "/"})
    public ResponseEntity<List<AddressDTO>> getAllAddresses() throws NoSuchAddressException, NoSuchStudentException, NoSuchGroupException {
        List<AddressEntity> addresses = addressService.findAll();
        Link link = linkTo(methodOn(AddressController.class).getAllAddresses()).withSelfRel();
        List<AddressDTO> addressDTO = new ArrayList<>();
        for (AddressEntity address : addresses) {
            Link selfLink = new Link(link.getHref() + address.getId()).withSelfRel();
            AddressDTO dto = new AddressDTO(address, selfLink);
            addressDTO.add(dto);
        }
        return new ResponseEntity<>(addressDTO, HttpStatus.OK);
    }

    @GetMapping({"/{addressId}"})
    public ResponseEntity<AddressEntity> getAddressById(@PathVariable Long addressId) throws NoSuchAddressException, NoSuchStudentException {
        return  new ResponseEntity<>(addressService.findById(addressId), HttpStatus.OK);
    }

    @GetMapping("/city/{city}")
    public ResponseEntity<List<AddressEntity>> getAddressByCity(@PathVariable String city) {
       return new ResponseEntity<>(addressService.findAddressByCity(city), HttpStatus.OK);
    }

    @PutMapping("")
    public ResponseEntity<AddressEntity> updateAddress(AddressEntity uAddress) throws NoSuchAddressException {
        AddressEntity address = addressService.update(uAddress);
        return new ResponseEntity<>(address, HttpStatus.OK);
    }

    @PostMapping("")
    public ResponseEntity<AddressEntity> addAddress(@RequestBody AddressEntity address) {
        AddressEntity addressEntity = addressService.save(address);
        return new ResponseEntity<>(addressEntity, HttpStatus.OK);
    }

    @DeleteMapping("")
    public ResponseEntity deleteStudent(@RequestBody AddressEntity address) {
        addressService.delete(address);
        return new ResponseEntity(HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity deleteStudentById(@PathVariable Long id) {
        addressService.deleteById(id);
        return new ResponseEntity(HttpStatus.OK);
    }
}
