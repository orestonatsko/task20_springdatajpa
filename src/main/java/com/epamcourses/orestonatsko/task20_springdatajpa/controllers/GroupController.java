package com.epamcourses.orestonatsko.task20_springdatajpa.controllers;

import com.epamcourses.orestonatsko.task20_springdatajpa.dto.GroupDTO;
import com.epamcourses.orestonatsko.task20_springdatajpa.exceptions.NoSuchAddressException;
import com.epamcourses.orestonatsko.task20_springdatajpa.exceptions.NoSuchGroupException;
import com.epamcourses.orestonatsko.task20_springdatajpa.exceptions.NoSuchStudentException;
import com.epamcourses.orestonatsko.task20_springdatajpa.model.GroupEntity;
import com.epamcourses.orestonatsko.task20_springdatajpa.services.GroupService;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

@RestController
@RequestMapping("/group")
public class GroupController {

    private GroupService groupService;

    public GroupController(GroupService groupService) {
        this.groupService = groupService;
    }

    @GetMapping("")
    public ResponseEntity<List<GroupDTO>> getAllGroups() throws NoSuchGroupException, NoSuchStudentException, NoSuchAddressException {
        return new ResponseEntity<>(createGroupDTOList(groupService.findAll()), HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<GroupDTO> getGroupById(@PathVariable Long id) throws NoSuchGroupException, NoSuchStudentException, NoSuchAddressException {
        return new ResponseEntity<>(createGroupDTO(groupService.findById(id)), HttpStatus.OK);
    }

    @GetMapping("/student/{studentId}")
    public ResponseEntity<GroupDTO> getGroupByStudentId(@PathVariable Long studentId) throws NoSuchStudentException, NoSuchGroupException, NoSuchAddressException {
        GroupEntity group = groupService.getGroupByStudentId(studentId);
        return new ResponseEntity<>(createGroupDTO(group), HttpStatus.OK);
    }

    @PutMapping("")
    public ResponseEntity<GroupDTO> updateGroup(@RequestBody GroupEntity group) throws NoSuchGroupException, NoSuchStudentException, NoSuchAddressException {
        return new ResponseEntity<>(createGroupDTO(groupService.update(group)), HttpStatus.OK);
    }

    @PostMapping("")
    public ResponseEntity<GroupDTO> addGroup(@RequestBody GroupEntity group) throws NoSuchStudentException, NoSuchAddressException, NoSuchGroupException {
        return new ResponseEntity<>(createGroupDTO(groupService.save(group)), HttpStatus.CREATED);

    }

    @DeleteMapping("")
    public ResponseEntity deleteGroup(@RequestBody GroupEntity group) {
        groupService.delete(group);
        return new ResponseEntity(HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity deleteGroupById(@PathVariable Long id) {
        groupService.deleteById(id);
        return new ResponseEntity(HttpStatus.OK);
    }

    private GroupDTO createGroupDTO(GroupEntity group) throws NoSuchGroupException, NoSuchStudentException, NoSuchAddressException {
        Link selfLink = linkTo(methodOn(GroupController.class).getGroupById(group.getId())).withSelfRel();
        return new GroupDTO(group, selfLink);
    }

    private List<GroupDTO> createGroupDTOList(List<GroupEntity> groups) throws NoSuchGroupException, NoSuchStudentException, NoSuchAddressException {
        List<GroupDTO> groupDTOList = new ArrayList<>();
        for (GroupEntity group : groups) {
            GroupDTO groupDTO = createGroupDTO(group);
            groupDTOList.add(groupDTO);
        }
        return groupDTOList;
    }


}
