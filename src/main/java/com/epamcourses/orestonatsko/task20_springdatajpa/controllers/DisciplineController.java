package com.epamcourses.orestonatsko.task20_springdatajpa.controllers;

import com.epamcourses.orestonatsko.task20_springdatajpa.dto.DisciplineDTO;
import com.epamcourses.orestonatsko.task20_springdatajpa.model.DisciplineEntity;
import com.epamcourses.orestonatsko.task20_springdatajpa.repositories.NoSuchDisciplineException;
import com.epamcourses.orestonatsko.task20_springdatajpa.services.DisciplineService;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

@Controller
@RequestMapping("/discipline")
public class DisciplineController {
    private DisciplineService disciplineService;

    public DisciplineController(DisciplineService disciplineService) {
        this.disciplineService = disciplineService;
    }

    @GetMapping({"", "/"})
    public ResponseEntity<List<DisciplineDTO>> getAllDisciplines() {
        List<DisciplineEntity> disciplines = disciplineService.findAll();
        return new ResponseEntity<>(createDisciplineDTOList(disciplines), HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<DisciplineDTO> getDisciplineById(@PathVariable Long id) {
        try {
            DisciplineEntity discipline = disciplineService.findById(id);
            return new ResponseEntity<>(createDisciplineDTO(discipline), HttpStatus.OK);
        } catch (NoSuchDisciplineException e) {
            System.out.println("~~NoSuchDisciplineException: DisciplineController.getDisciplineById method~~");
        }
        return null;
    }

    @PostMapping("")
    public ResponseEntity<DisciplineDTO> addDiscipline(@RequestBody DisciplineEntity discipline) {
        DisciplineEntity disciplineEntity = disciplineService.save(discipline);
        return new ResponseEntity<>(createDisciplineDTO(disciplineEntity), HttpStatus.CREATED);
    }

    @PutMapping("")
    public ResponseEntity<DisciplineDTO> updateDiscipline(@RequestBody DisciplineEntity discipline) throws NoSuchDisciplineException {
        disciplineService.update(discipline);
        DisciplineEntity uDiscipline = disciplineService.findById(discipline.getId());
        return new ResponseEntity<>(createDisciplineDTO(uDiscipline), HttpStatus.OK);
    }

    @DeleteMapping("")
    public ResponseEntity deleteDiscipline(@RequestBody DisciplineEntity discipline) {
        disciplineService.delete(discipline);
        return new ResponseEntity(HttpStatus.OK);
    }

    @DeleteMapping("{id}")
    public ResponseEntity deleteById(@PathVariable Long id) {
        disciplineService.deleteById(id);
        return new ResponseEntity(HttpStatus.OK);
    }

    private DisciplineDTO createDisciplineDTO(DisciplineEntity discipline) {
        Link selfLink = linkTo(methodOn(DisciplineController.class).getDisciplineById(discipline.getId())).withSelfRel();
        return new DisciplineDTO(discipline, selfLink);
    }

    private List<DisciplineDTO> createDisciplineDTOList(List<DisciplineEntity> disciplines) {
        return disciplines.stream().map(this::createDisciplineDTO).collect(Collectors.toList());
    }


}
