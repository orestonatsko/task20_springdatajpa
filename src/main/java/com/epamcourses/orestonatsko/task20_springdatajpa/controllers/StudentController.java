package com.epamcourses.orestonatsko.task20_springdatajpa.controllers;

import com.epamcourses.orestonatsko.task20_springdatajpa.dto.StudentDTO;
import com.epamcourses.orestonatsko.task20_springdatajpa.exceptions.NoSuchAddressException;
import com.epamcourses.orestonatsko.task20_springdatajpa.exceptions.NoSuchGroupException;
import com.epamcourses.orestonatsko.task20_springdatajpa.exceptions.NoSuchStudentException;
import com.epamcourses.orestonatsko.task20_springdatajpa.model.StudentEntity;
import com.epamcourses.orestonatsko.task20_springdatajpa.services.StudentService;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

@RestController
@RequestMapping("/student")
public class StudentController {
    private StudentService studentService;

    public StudentController(StudentService studentService) {
        this.studentService = studentService;
    }

    @GetMapping("/address/{addressId}")
    public ResponseEntity<List<StudentDTO>> getStudentsByAddressId(@PathVariable Long addressId) throws NoSuchAddressException, NoSuchStudentException, NoSuchGroupException {
        List<StudentEntity> students = studentService.findStudentsByAddressId(addressId);
        return new ResponseEntity<>(createStudentDTOList(students), HttpStatus.OK);
    }

    @GetMapping("/group/{groupId}")
    public ResponseEntity<List<StudentDTO>> getStudentsByGroupId(@PathVariable Long groupId) throws NoSuchStudentException, NoSuchGroupException, NoSuchAddressException {
        List<StudentEntity> students = studentService.findStudentsByGroupId(groupId);
        return new ResponseEntity<>(createStudentDTOList(students), HttpStatus.OK);
    }

    @GetMapping({"", "/"})
    public ResponseEntity<List<StudentDTO>> getAllStudents() throws NoSuchStudentException, NoSuchGroupException, NoSuchAddressException {
        List<StudentEntity> students = studentService.findAll();
        return new ResponseEntity<>(createStudentDTOList(students), HttpStatus.OK);
    }

    @GetMapping("/name/{like}")
    public ResponseEntity<List<StudentDTO>> getStudentByFirstNameLike(@PathVariable String like) throws NoSuchStudentException, NoSuchGroupException, NoSuchAddressException {
        List<StudentEntity> studentEntityList = studentService.getStudentByFirstNameLike(like);
        return new ResponseEntity<>(createStudentDTOList(studentEntityList), HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<StudentDTO> getStudentById(@PathVariable Long id) throws NoSuchStudentException, NoSuchAddressException, NoSuchGroupException {
        StudentEntity student = studentService.findById(id);
        return new ResponseEntity<>(createStudentDTO(student), HttpStatus.OK);
    }

    @PutMapping("/{id}")
    public ResponseEntity<StudentDTO> updateStudent(@RequestBody StudentEntity student) throws NoSuchAddressException, NoSuchStudentException, NoSuchGroupException {
        StudentEntity studentEntity = studentService.update(student);
        return new ResponseEntity<>(createStudentDTO(studentEntity), HttpStatus.OK);
    }

    @PostMapping("")
    public ResponseEntity<StudentDTO> addStudent(@RequestBody StudentEntity student) throws NoSuchStudentException, NoSuchAddressException, NoSuchGroupException {
        StudentEntity studentEntity = studentService.save(student);
        Link link = linkTo(methodOn(StudentController.class).getStudentById(studentEntity.getId())).withSelfRel();
        StudentDTO studentDTO = new StudentDTO(student, link);
        return new ResponseEntity<>(studentDTO, HttpStatus.CREATED);
    }

    @DeleteMapping("")
    public ResponseEntity delete(@RequestBody StudentEntity student) {
        studentService.delete(student);
        return new ResponseEntity(HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity deleteById(@PathVariable Long id) {
        studentService.deleteById(id);
        return new ResponseEntity(HttpStatus.OK);
    }

    private StudentDTO createStudentDTO(StudentEntity studentEntity) throws NoSuchStudentException, NoSuchAddressException, NoSuchGroupException {
        Link link = linkTo(methodOn(StudentController.class).getStudentById(studentEntity.getId())).withSelfRel();
        return new StudentDTO(studentEntity, link);
    }

    private List<StudentDTO> createStudentDTOList(List<StudentEntity> students) throws NoSuchStudentException, NoSuchAddressException, NoSuchGroupException {
        List<StudentDTO> studentDTOList = new ArrayList<>();
        for (StudentEntity student : students) {
            StudentDTO dto = createStudentDTO(student);
            studentDTOList.add(dto);
        }
        return studentDTOList;
    }

}
