package com.epamcourses.orestonatsko.task20_springdatajpa.services;

import com.epamcourses.orestonatsko.task20_springdatajpa.exceptions.NoSuchAddressException;
import com.epamcourses.orestonatsko.task20_springdatajpa.exceptions.NoSuchStudentException;
import com.epamcourses.orestonatsko.task20_springdatajpa.model.AddressEntity;

import java.util.List;

public interface AddressService extends CrudService<AddressEntity, Long> {

    @Override
    AddressEntity findById(Long id) throws NoSuchAddressException;

    AddressEntity findAddressByStudentId(Long id) throws NoSuchStudentException;

    List<AddressEntity> findAddressByCity(String city);

    @Override
    AddressEntity update(AddressEntity address) throws NoSuchAddressException;
}
