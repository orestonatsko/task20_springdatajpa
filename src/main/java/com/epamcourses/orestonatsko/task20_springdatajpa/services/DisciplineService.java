package com.epamcourses.orestonatsko.task20_springdatajpa.services;

import com.epamcourses.orestonatsko.task20_springdatajpa.model.DisciplineEntity;
import com.epamcourses.orestonatsko.task20_springdatajpa.repositories.NoSuchDisciplineException;

public interface DisciplineService extends CrudService<DisciplineEntity, Long> {
    @Override
    DisciplineEntity findById(Long aLong) throws NoSuchDisciplineException;

    @Override
    DisciplineEntity update(DisciplineEntity obj) throws NoSuchDisciplineException;
}
