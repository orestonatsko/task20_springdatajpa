package com.epamcourses.orestonatsko.task20_springdatajpa.services.implementation;

import com.epamcourses.orestonatsko.task20_springdatajpa.exceptions.NoSuchGroupException;
import com.epamcourses.orestonatsko.task20_springdatajpa.exceptions.NoSuchStudentException;
import com.epamcourses.orestonatsko.task20_springdatajpa.model.GroupEntity;
import com.epamcourses.orestonatsko.task20_springdatajpa.model.StudentEntity;
import com.epamcourses.orestonatsko.task20_springdatajpa.repositories.GroupRepository;
import com.epamcourses.orestonatsko.task20_springdatajpa.repositories.StudentRepository;
import com.epamcourses.orestonatsko.task20_springdatajpa.services.GroupService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

import static com.epamcourses.orestonatsko.task20_springdatajpa.services.implementation.StudentServiceImpl.getStudent;

@Service
public class GroupServiceImpl implements GroupService {
    private GroupRepository groupRepository;
    private StudentRepository studentRepository;

    public GroupServiceImpl(GroupRepository groupRepository, StudentRepository studentRepository) {
        this.groupRepository = groupRepository;
        this.studentRepository = studentRepository;
    }

    @Override
    public List<GroupEntity> findAll() {
        return groupRepository.findAll();
    }

    @Override
    public GroupEntity findById(Long id) throws NoSuchGroupException {
        return getGroup(id, groupRepository);
    }

    @Override
    public GroupEntity getGroupByStudentId(Long studentId) throws NoSuchStudentException {
        StudentEntity student = getStudent(studentId, studentRepository);
        return student.getGroup();
    }

    @Override
    public GroupEntity save(GroupEntity group) {
        return groupRepository.save(group);
    }

    @Override
    public GroupEntity update(GroupEntity uGroup) throws NoSuchGroupException {
        GroupEntity group = getGroup(uGroup.getId(), groupRepository);
        group.setName(uGroup.getName());
        return group;
    }

    @Override
    public void delete(GroupEntity group) {
        groupRepository.delete(group);
    }

    @Override
    public void deleteById(Long id) {
        groupRepository.deleteById(id);

    }

     static GroupEntity getGroup(Long id, GroupRepository groupRepository) throws NoSuchGroupException {
        Optional result = groupRepository.findById(id);
        GroupEntity groupEntity;
        if(result.isPresent()){
            groupEntity = (GroupEntity) result.get();
        } else {
            throw new NoSuchGroupException();
        }
        return groupEntity;
    }
}
