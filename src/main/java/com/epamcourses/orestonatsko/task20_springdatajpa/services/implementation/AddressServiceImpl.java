package com.epamcourses.orestonatsko.task20_springdatajpa.services.implementation;


import com.epamcourses.orestonatsko.task20_springdatajpa.exceptions.NoSuchAddressException;
import com.epamcourses.orestonatsko.task20_springdatajpa.exceptions.NoSuchStudentException;
import com.epamcourses.orestonatsko.task20_springdatajpa.model.AddressEntity;
import com.epamcourses.orestonatsko.task20_springdatajpa.model.StudentEntity;
import com.epamcourses.orestonatsko.task20_springdatajpa.repositories.AddressRepository;
import com.epamcourses.orestonatsko.task20_springdatajpa.repositories.StudentRepository;
import com.epamcourses.orestonatsko.task20_springdatajpa.services.AddressService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

import static com.epamcourses.orestonatsko.task20_springdatajpa.services.implementation.StudentServiceImpl.getStudent;

@Service
public class AddressServiceImpl implements AddressService {
    private AddressRepository addressRepository;
    private StudentRepository studentRepository;

    public AddressServiceImpl(AddressRepository addressRepository, StudentRepository studentRepository) {
        this.addressRepository = addressRepository;
        this.studentRepository = studentRepository;
    }

    @Override
    public AddressEntity findAddressByStudentId(Long studentId) throws NoSuchStudentException {
        StudentEntity student = getStudent(studentId, studentRepository);
        return student.getAddress();
    }

    @Override
    public List<AddressEntity> findAddressByCity(String city) {
        return addressRepository.findAddressByCity(city);
    }

    @Override
    public List<AddressEntity> findAll() {
        return addressRepository.findAll();
    }

    @Override
    public AddressEntity findById(Long id) throws NoSuchAddressException {
        return getAddress(id, addressRepository);
    }

    @Override
    public AddressEntity save(AddressEntity address) {
        return addressRepository.save(address);
    }

    @Transactional
    @Override
    public AddressEntity update(AddressEntity address) throws NoSuchAddressException {
        AddressEntity uAddress = getAddress(address.getId(), addressRepository);
        uAddress.setCity(address.getCity());
        uAddress.setStreet(address.getStreet());
        uAddress.setNumbBuilding(address.getNumbBuilding());
        return uAddress;
    }

    @Transactional
    @Override
    public void delete(AddressEntity address) {
        addressRepository.delete(address);
    }

    @Transactional
    @Override
    public void deleteById(Long id) {
        addressRepository.deleteById(id);
    }

    static AddressEntity getAddress(Long addressId, AddressRepository addressRepository) throws NoSuchAddressException {
        Optional result = addressRepository.findById(addressId);
        AddressEntity address;
        if(result.isPresent()){
            address = (AddressEntity) result.get();
        }else {
            throw new NoSuchAddressException();
        }
        return address;
    }
}
