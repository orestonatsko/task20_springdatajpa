package com.epamcourses.orestonatsko.task20_springdatajpa.services.implementation;

import com.epamcourses.orestonatsko.task20_springdatajpa.exceptions.NoSuchAddressException;
import com.epamcourses.orestonatsko.task20_springdatajpa.exceptions.NoSuchGroupException;
import com.epamcourses.orestonatsko.task20_springdatajpa.exceptions.NoSuchStudentException;
import com.epamcourses.orestonatsko.task20_springdatajpa.model.AddressEntity;
import com.epamcourses.orestonatsko.task20_springdatajpa.model.GroupEntity;
import com.epamcourses.orestonatsko.task20_springdatajpa.model.StudentEntity;
import com.epamcourses.orestonatsko.task20_springdatajpa.repositories.AddressRepository;
import com.epamcourses.orestonatsko.task20_springdatajpa.repositories.GroupRepository;
import com.epamcourses.orestonatsko.task20_springdatajpa.repositories.StudentRepository;
import com.epamcourses.orestonatsko.task20_springdatajpa.services.StudentService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

import static com.epamcourses.orestonatsko.task20_springdatajpa.services.implementation.AddressServiceImpl.getAddress;
import static com.epamcourses.orestonatsko.task20_springdatajpa.services.implementation.GroupServiceImpl.getGroup;

@Service
public class StudentServiceImpl implements StudentService {
    private StudentRepository studentRepository;
    private AddressRepository addressRepository;
    private GroupRepository groupRepository;

    public StudentServiceImpl(StudentRepository studentRepository, AddressRepository addressRepository, GroupRepository groupRepository) {
        this.studentRepository = studentRepository;
        this.addressRepository = addressRepository;
        this.groupRepository = groupRepository;
    }

    @Override
    public List<StudentEntity> findStudentsByAddressId(Long addressId) throws NoSuchAddressException {
        AddressEntity address = getAddress(addressId, addressRepository);
        return address.getStudents();
    }

    @Override
    public List<StudentEntity> findStudentsByGroupId(Long groupId) throws NoSuchGroupException {
        GroupEntity group = getGroup(groupId, groupRepository);
        return group.getStudents();
    }

    @Override
    public List<StudentEntity> findAll() {
        return studentRepository.findAll();
    }

    @Override
    public StudentEntity findById(Long id) throws NoSuchStudentException {
        return getStudent(id, studentRepository);
    }

    @Transactional
    @Override
    public StudentEntity save(StudentEntity student) {
        return studentRepository.save(student);
    }

    @Transactional
    @Override
    public StudentEntity update(StudentEntity student) throws NoSuchStudentException {
        StudentEntity uStudent = getStudent(student.getId(), studentRepository);
        uStudent.setFirstName(student.getFirstName());
        uStudent.setSecondName(student.getSecondName());
        uStudent.setGroup(student.getGroup());
        uStudent.setAddress(student.getAddress());
        return uStudent;
    }

    @Override
    public List<StudentEntity> getStudentByFirstNameLike(String like) {
        return studentRepository.findByFirstNameLike(like + "%");
    }

    @Transactional
    @Override
    public void delete(StudentEntity student) {
        studentRepository.delete(student);
    }

    @Transactional
    @Override
    public void deleteById(Long id) {
        studentRepository.deleteById(id);
    }

    static StudentEntity getStudent(Long studentId, StudentRepository studentRepository) throws NoSuchStudentException {
        Optional result = studentRepository.findById(studentId);
        StudentEntity student;
        if (result.isPresent()) {
            student = (StudentEntity) result.get();
        } else {
            throw new NoSuchStudentException();
        }
        return student;
    }
}
