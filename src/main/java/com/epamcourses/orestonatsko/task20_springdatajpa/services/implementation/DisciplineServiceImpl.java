package com.epamcourses.orestonatsko.task20_springdatajpa.services.implementation;

import com.epamcourses.orestonatsko.task20_springdatajpa.model.DisciplineEntity;
import com.epamcourses.orestonatsko.task20_springdatajpa.repositories.DisciplineRepository;
import com.epamcourses.orestonatsko.task20_springdatajpa.repositories.NoSuchDisciplineException;
import com.epamcourses.orestonatsko.task20_springdatajpa.services.DisciplineService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class DisciplineServiceImpl implements DisciplineService {
    private DisciplineRepository disciplineRepository;

    public DisciplineServiceImpl(DisciplineRepository disciplineRepository) {
        this.disciplineRepository = disciplineRepository;
    }

    @Override
    public List<DisciplineEntity> findAll() {
        return disciplineRepository.findAll();
    }

    @Override
    public DisciplineEntity findById(Long id) throws NoSuchDisciplineException {
        return getDiscipline(id, disciplineRepository);
    }

    @Override
    public DisciplineEntity save(DisciplineEntity discipline) {
        return disciplineRepository.save(discipline);
    }

    @Override
    public DisciplineEntity update(DisciplineEntity discipline) throws NoSuchDisciplineException {
        DisciplineEntity uDiscipline = getDiscipline(discipline.getId(), disciplineRepository);
        uDiscipline.setDiscipline(discipline.getDiscipline());
        return uDiscipline;
    }

    @Override
    public void delete(DisciplineEntity discipline) {
        disciplineRepository.delete(discipline);
    }

    @Override
    public void deleteById(Long id) {
        disciplineRepository.deleteById(id);
    }

    static DisciplineEntity getDiscipline(Long id, DisciplineRepository disciplineRepository) throws NoSuchDisciplineException {
        DisciplineEntity discipline;
        Optional result = disciplineRepository.findById(id);
        if (result.isPresent()) {
            discipline = (DisciplineEntity) result.get();
        } else {
            throw new NoSuchDisciplineException();
        }
        return discipline;
    }
}
