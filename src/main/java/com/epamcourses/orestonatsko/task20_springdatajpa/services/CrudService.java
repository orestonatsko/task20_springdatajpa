package com.epamcourses.orestonatsko.task20_springdatajpa.services;

import java.util.List;

public interface CrudService<T, ID> {

    List<T> findAll();

    T findById(ID id) throws Exception;

    T save(T obj);

    T update(T obj) throws Exception;

    void delete(T obj);

    void deleteById(ID id);

}
