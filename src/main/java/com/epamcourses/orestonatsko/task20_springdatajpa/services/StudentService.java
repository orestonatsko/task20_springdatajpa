package com.epamcourses.orestonatsko.task20_springdatajpa.services;

import com.epamcourses.orestonatsko.task20_springdatajpa.exceptions.NoSuchAddressException;
import com.epamcourses.orestonatsko.task20_springdatajpa.exceptions.NoSuchGroupException;
import com.epamcourses.orestonatsko.task20_springdatajpa.exceptions.NoSuchStudentException;
import com.epamcourses.orestonatsko.task20_springdatajpa.model.StudentEntity;

import java.util.List;

public interface StudentService extends CrudService<StudentEntity, Long> {


    @Override
    StudentEntity findById(Long id) throws NoSuchStudentException;

    List<StudentEntity> findStudentsByGroupId(Long groupId) throws NoSuchGroupException;

    List<StudentEntity> findStudentsByAddressId(Long addressId) throws NoSuchAddressException;

    @Override
    StudentEntity update(StudentEntity student) throws NoSuchStudentException;

    List<StudentEntity> getStudentByFirstNameLike(String like);
}
