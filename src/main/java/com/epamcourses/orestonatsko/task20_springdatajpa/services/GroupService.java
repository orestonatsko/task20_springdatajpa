package com.epamcourses.orestonatsko.task20_springdatajpa.services;

import com.epamcourses.orestonatsko.task20_springdatajpa.exceptions.NoSuchGroupException;
import com.epamcourses.orestonatsko.task20_springdatajpa.exceptions.NoSuchStudentException;
import com.epamcourses.orestonatsko.task20_springdatajpa.model.GroupEntity;

public interface GroupService extends CrudService<GroupEntity, Long> {

    @Override
    GroupEntity findById(Long id) throws NoSuchGroupException;

    @Override
    GroupEntity update(GroupEntity group) throws NoSuchGroupException;

    GroupEntity getGroupByStudentId(Long groupId) throws NoSuchStudentException;

}
