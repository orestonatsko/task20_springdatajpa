package com.epamcourses.orestonatsko.task20_springdatajpa;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Task20SpringDataJpaApplication {

    public static void main(String[] args) {
        SpringApplication.run(Task20SpringDataJpaApplication.class, args);
    }

}

